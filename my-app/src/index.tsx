import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Login from "./App";
import Table from "./Table";
import Upload from "./Upload";
import DataPicker from "./DataPicker";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import at_context from "./context";
import { createBrowserHistory } from "history";
import interceptors from "./api/interceptors";
import { useState } from "react";

function AppRender() {
  const history = createBrowserHistory();
  const [isAuth, setAuth] = useState(false);
  interceptors.setupInterceptors(history);

  return (
    <at_context.Provider value={{ isAuth, setAuth }}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={DataPicker} />
          <Route path="/table" component={Table} />
          <Route path="/upload" component={Upload} />
        </Switch>
      </BrowserRouter>
    </at_context.Provider>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <AppRender />
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
