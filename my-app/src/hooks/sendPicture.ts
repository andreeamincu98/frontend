// import axios, { AxiosError } from "axios";
// import instance from "../api/instance"

// export default {
//     async sendPic(pic: Blob | MediaSource): Promise<any> {
//         try{
//             const resp = await instance.post("/evaluate", {pic});
//             console.log(resp)
//             return resp.data;
//         } catch (error) {
//             const err = error as AxiosError
//             if (err.response) {
//                 console.log(err.response.status)
//                 console.log(err.response.data)
//             }
//             throw error;
//         }
//     }
// }

import { useState, useEffect, Dispatch, SetStateAction } from "react";

const getStorageValue = (key: string, defaultValue: string) => {
  const saved = localStorage.getItem(key);
  return saved || defaultValue;
};

const useLocalStorage = (
  key: string,
  defaultValue: string
): [string, Dispatch<SetStateAction<string>>] => {
  const [value, setValue] = useState(() => {
    return getStorageValue(key, defaultValue);
  });

  useEffect(() => {
    localStorage.setItem(key, value);
  }, [key, value]);

  return [value, setValue];
};

export default useLocalStorage;
