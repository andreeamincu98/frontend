import axios, { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';
import { env_var } from "../Environment/environment"
let headers = {}
const requestConfig:AxiosRequestConfig={

    baseURL: env_var.URL,
    headers: {}

}
const instance = axios.create(requestConfig);

export default instance;