import axios from "axios";
import { gcp_env_var } from "../Environment/environment"

const instance = axios.create({
  baseURL: gcp_env_var.URL,
  headers: { "content-type": "application/json;charset=UTF-8" },
});

export default instance;
