import { useState, useEffect } from "react";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import useLocalStorage from "./hooks/sendPicture";
import axiosInstance from "./api/instance";

const FileInput = () => {
  async function get(url: string) {
    try {
      const response = await fetch(url, {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        headers: {
          "Content-Type": "image/png",
        },
      });
      const blob = await response.blob();
      return [URL.createObjectURL(blob), null];
    } catch (error) {
      console.error(`get: error occurred ${error}`);
      return [null, error];
    }
  }

  const [selectedImage, setSelectedImage] = useState<Blob>();
  const [imageUrl, setImageUrl] = useState("");
  const [images, setImages] = useLocalStorage("images", JSON.stringify([]));
  const [file, setFile] = useState<File>();

  useEffect(() => {
    async function fetchData() {
      if (selectedImage) {
        var formData = new FormData();
        formData.append("image", selectedImage);
        axiosInstance.post("evaluate", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        setImageUrl(URL.createObjectURL(selectedImage));
      }
    }
    fetchData();
  }, [selectedImage]);

  return (
    <>
      <input
        accept="image/*"
        type="file"
        id="select-image"
        style={{ display: "none" }}
        onChange={(e) => setSelectedImage(e.target.files![0])}
      />
      <label htmlFor="select-image">
        <Button
          variant="contained"
          color="default"
          component="span"
          style={{ marginLeft: "570px", marginTop: "25px" }}
        >
          Upload Image
        </Button>
      </label>
      {imageUrl && selectedImage && (
        <Box>
          <Box
            mt={5}
            ml={22}
            textAlign="center"
            bgcolor={"#eeeeee"}
            width={"550px"}
            height={"550px"}
          >
            <div style={{ paddingTop: "80px" }}>Image Preview:</div>
            <img src={imageUrl} style={{ paddingTop: "20px" }} height="300px" />
          </Box>
        </Box>
      )}
    </>
  );
};

export default FileInput;
