import { DataGrid, GridColDef } from "@mui/x-data-grid";
import Box from "@mui/material/Box";
import * as React from "react";
import { Component } from "react";
import { Button } from "@mui/material";

const columns: GridColDef[] = [
  { field: "imageName", headerName: "Image name", width: 250 },
  { field: "size", headerName: "Size", type: "number", width: 200 },
  {
    field: "recognitionResult",
    headerName: "Recognition result",
    type: "number",
    width: 200,
  },
  {
    field: "imageDownloadLink",
    headerName: "Image Download Link",
    width: 300,
  },
];

const rows = [
  {
    id: 1,
    imageName: "a",
    size: 15,
    recognitionResult: 100,
    imageDownloadLink: "a",
  },
  {
    id: 2,
    imageName: "b",
    size: 58,
    recognitionResult: 45,
    imageDownloadLink: "b",
  },
  {
    id: 3,
    imageName: "c",
    size: 46,
    recognitionResult: 15,
    imageDownloadLink: "c",
  },
  {
    id: 4,
    imageName: "d",
    size: 13,
    recognitionResult: 4,
    imageDownloadLink: "d",
  },
  {
    id: 5,
    imageName: "e",
    size: 24,
    recognitionResult: 12,
    imageDownloadLink: "e",
  },
  {
    id: 6,
    imageName: "f",
    size: 465,
    recognitionResult: 56,
    imageDownloadLink: "f",
  },
  {
    id: 7,
    imageName: "g",
    size: 98,
    recognitionResult: 12,
    imageDownloadLink: "g",
  },
  {
    id: 8,
    imageName: "h",
    size: 32,
    recognitionResult: 15,
    imageDownloadLink: "h",
  },
  {
    id: 9,
    imageName: "i",
    size: 99,
    recognitionResult: 85,
    imageDownloadLink: "i",
  },
];

class Table extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.buttonClicked = this.buttonClicked.bind(this);
  }

  buttonClicked = () => {
    this.props.history.push("/datapicker");
  };

  render() {
    return (
      <Box
        sx={{
          ml: 20,
          mr: 20,
          mt: 10,
          mb: 10,
          display: "flex",
          flexDirection: "column",
          alignItems: "right",
        }}
      >
        <div style={{ height: 580, width: "100%" }}>
          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            checkboxSelection
            hideFooterPagination={true}
          />
        </div>
        <Button
          type="submit"
          style={{ width: "200px" }}
          variant="contained"
          color="success"
          sx={{ mt: 3, mb: 2, ml: 80 }}
          onClick={this.buttonClicked}
        >
          {" "}
          Next page
        </Button>
      </Box>
    );
  }
}
export default Table;