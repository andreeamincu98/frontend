import * as React from 'react';
import TextField from '@mui/material/TextField';
import DateRangePicker, { DateRange } from '@mui/lab/DateRangePicker';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import Box from '@mui/material/Box';
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import { Button } from "@mui/material";
import axios from "./api/gcpInstance";
import { useEffect, useState } from "react";
import { margin } from '@mui/system';

export default function BasicDateRangePicker() {
  const [value, setValue] = React.useState<DateRange<Date>>([null, null]);

  const columns: GridColDef[] = [
    { field: "start_date", headerName: "Start Date", type: "date", width: 250 },
    { field: "end_date", headerName: "End Date", type: "date", width: 250 },
  ];

  const rows = [
    {
      id: 1,
      start_date: value[0]?.toDateString(),
      end_date: value[1]?.toDateString(),
    },
  ];

  // type rowDataPicker = {
  //     id: number,
  //     start_date: Date,
  //     end_date: Date,
  // };

  // type allRowsDataPicker = {
  //   allrowsdatapicker: Array<rowDataPicker>;
  // };

  // const DataPickerForm = [{
  //   allrowsdatapicker : Array<rowDataPicker>,
  // },];



  // useEffect(() => {

  //   axios
  //   .get("create" , {params:{start_date : String(value[0]), end_date: String(value[1])}})
  //   .then ((response) => {
  //     if (response.status !== 200)
  //     {
  //       console.log("Nu s-a putut incarca in cloud");
  //     }
  //   })
  //   .catch((e) => {
  //     console.log("Error ceva!");
  //   })
  // })

  return (
    <Box>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <div style={{ display: 'flex', flexDirection: 'row', marginTop: '100px', marginLeft: "500px" }}>
          <DateRangePicker
            startText="Check-in"
            endText="Check-out"
            value={value}
            onChange={(newValue) => {
              setValue(newValue);
            }}
            renderInput={(startProps, endProps) => (
              <React.Fragment >
                <TextField {...startProps} />
                <Box sx={{ mx: 2 }}> to </Box>
                <TextField {...endProps} />
              </React.Fragment>
            )}
          />
        </div>
      </LocalizationProvider>

      <Button
        type="submit"
        style={{ width: "200px" }}
        variant="contained"
        sx={{ mt: 3, mb: 2, ml: 80 }}
        onClick={() => {
          if (value[0] !== null && value[1] !== null) {
            axios
              .get("create", { params: { start_date: value[0]?.toDateString(), end_date: value[1]?.toDateString() } })
              .then((response) => {
                if (response.status !== 200) {
                  console.log("Nu s-a putut incarca in cloud");
                }
              })
              .catch((e) => {
                console.log("Error ceva!");
              })
          }
        }
        }
      >
        {" "}
        Create Schedule
      </Button>

      <div style={{ height: 400, width: "80%", marginTop: 50, marginLeft: 150 }}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={10}
          checkboxSelection
          hideFooterPagination={true}
        />
      </div>
    </Box>
  );
}